import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {

	public static String URL;
	// cookie信息
	public static String COOKIE;

	public static void main(String[] args) throws IOException {
		// 读取源文件的url和cookie
		getResources();

		// 初始化存储课堂完成部分链接的列表
		ArrayList<String> baseList = new ArrayList<String>();

		// // 初始化一个列表存储解析课堂完成部分的html文件的document对象
		// ArrayList<Document> baseActives = new ArrayList<Document>(baseList.size());
		//
		// // 学生列表
		// ArrayList<Student> studentList = new ArrayList<>();

		// 手动设置cookies,解析html
		Document document = (Document) Jsoup.connect(URL).header("cookie", COOKIE).get();

		// 测试部分
		// if (document != null) {
		// // 获取课程名称节点
		// Element element = document.select(".color-66").first();
		// if (element == null) {
		// System.out.println("没有找到 .color-66 标签");
		// return;
		// }
		// // 取出课程名称
		// String userName = element.ownText();
		// System.out.println("课程名为：" + userName);
		// } else {
		// System.out.println("出错啦！");
		// }

		// 获取所有课堂完成部分的url
		if (document != null) {
			// 获取到所有活动的div
			Elements activDivs = ((Element) document).getElementsByClass("interaction-row");
			// 获取课堂完成部分活动的url
			for (int i = 0; i < activDivs.size(); i++) {
				if (activDivs.get(i).toString().contains("课堂完成")) {
					// 将属性data-url的值转为字符串
					String urlString = activDivs.get(i).attr("data-url").toString();
					// 把值加到baseList
					baseList.add(urlString);
				}
			}
		} else {
			System.out.println("出错啦！");
		}

		//学生信息
		ArrayList<Student> newStudentList=stuList(baseList);
		// 排序
		Collections.sort(newStudentList, new Student());
		// 写入
		write(newStudentList);
	}

	// 读取源文件的url和cookie
	public static void getResources() {
		// 从配置文件中加载配置项：使用Properties的load()方法
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("resources/config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 读取配置项中的结果：使用Properties的getProperty()方法
		URL = prop.getProperty("url");
		COOKIE = prop.getProperty("cookie");
	}

	public static ArrayList<Student> stuList(ArrayList<String> baseList) throws IOException {
		// 初始化一个列表存储解析课堂完成部分的html文件的document对象
		ArrayList<Document> baseActives = new ArrayList<Document>(baseList.size());
		// 学生列表
		ArrayList<Student> studentList = new ArrayList<>();
		// 存储解析课堂完成部分的html文件的document对象
		for (int i = 0; i < baseList.size(); i++) {
			// 解析课堂完成部分
			Document document1 = (Document) Jsoup.connect(baseList.get(i)).header("cookie", COOKIE).get();
			// 将解析后的document添加到baseActives
			baseActives.add(document1);
		}
		for (int j = 0; j < baseActives.size(); j++) {
			// 获取每个课堂完成部分网页里的class="homework-item"的div
			Elements baseStuDivs = ((Element) baseActives.get(j)).getElementsByClass("homework-item");
			for (int k = 0; k < baseStuDivs.size(); k++) {
				try {
					// 学生
					Student stu = new Student();
					// 设置学号
					stu.setId(baseStuDivs.get(k).child(0).child(1).child(1).text().toString());
					// 设置姓名
					stu.setName(baseStuDivs.get(k).child(0).child(1).child(0).text().toString());
					// 获得成绩文本
					String score = baseStuDivs.get(k).child(3).child(1).child(1).text();
					// 没被评分的分数为0.0
					if (baseStuDivs.get(k).child(3).child(0).child(1).text().contains("尚无评分")) {
						stu.setScore(0.0);
					}
					// 没有提交的分数为0.0
					else if (baseStuDivs.get(k).child(1).child(0).text().contains("未提交")) {
						stu.setScore(0.0);
					} else {
						// 设置成绩
						stu.setScore(Double.parseDouble(score.substring(0, score.length() - 2)));
					}
					studentList.add(stu);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		// 去重后的学生列表
		ArrayList<Student> newStudentList = new ArrayList<>();
		// 每个学生的总成绩
		Double totalScore;
		for (int i = 0; i < studentList.size(); i++) {
			// 初始化每个学生的总成绩
			totalScore = 0.0;
			// 学生
			Student student = new Student();
			for (int j = i + 1; j < studentList.size(); j++) {
				if (studentList.get(i).getName().contains(studentList.get(j).getName())) {
					// 计算成绩
					totalScore += studentList.get(j).getScore();
					// 删掉重复的
					studentList.remove(j);
				}
			}
			// 学生和学生的总成绩的学生对象
			student.setId(studentList.get(i).getId());
			student.setName(studentList.get(i).getName());
			student.setScore(totalScore);
			// 加入到新列表
			newStudentList.add(student);
		}
		return newStudentList;
	}

	public static void write(ArrayList<Student> newStudentList) throws FileNotFoundException {
		// 确定输出文件的目的地
		File file = new File("score.txt");
		// 创建指向文件的打印输出流
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file), true);
		// 输出数据
		double ave = 0.0;
		for (int j = 0; j < newStudentList.size(); j++) {
			ave += newStudentList.get(j).getScore();
		}
		ave = ave / newStudentList.size();
		printWriter.println("最高经验值" + newStudentList.get(0).getScore() + ",最低经验值"
				+ newStudentList.get(newStudentList.size() - 1).getScore() + ",平均经验值" + ave);
		for (int i = 0; i < newStudentList.size(); i++) {
			printWriter.println(newStudentList.get(i).toString());
		}
		// 关闭
		printWriter.close();
	}
}
