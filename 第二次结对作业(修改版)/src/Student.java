
import java.util.Comparator;

public class Student implements Comparator<Student>{
	public String id;
	public String name;
	public double score;
	
	public Student() {
		super();
	}
	
	public Student(String id, String name, double score) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
	}
	
	public  String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return id + "," + name + "," + score;
	}

	@Override
	public int compare(Student o1, Student o2) {
		// TODO Auto-generated method stub
		int sort1 = Integer.parseInt(o1.getId()) - Integer.parseInt(o2.getId());
		int sort2 = (int) (o2.getScore() - o1.getScore());
		return sort2==0?sort1:sort2;
	}
}
